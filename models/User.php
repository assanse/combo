<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;


class User extends ActiveRecord implements IdentityInterface
{
	public $rePassword;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function rules()
    {
        return [
            [['name', 'password', 'rePassword'], 'required'],
            [['name'], 'email'],
            [['name'], 'unique'],
            [['password', 'rePassword'], 'string', 'max' => 255],
	        ['rePassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают" ],
	        [['auth_key'], 'string'],
        ];
    }

	public function attributeLabels()
	{
		return [
			'name' => 'Email',
			'password' => 'Пароль',
			'rePassword' => 'Повторите пароль'
		];
    }

	public function beforeSave($insert)
	{
		parent::beforeSave($insert);
		$this->password = Yii::$app->security->generatePasswordHash($this->password);

		return true;
	}

	public function beforeDelete()
	{
		if($this->id == Yii::$app->user->id)
		{
			throw new NotFoundHttpException('Себя к сожалению нельзя', '403');
		}

		parent::beforeDelete();
		if(!empty($this->articles))
		{
			foreach($this->articles as $article)
			{
				$article->delete();
			}
		}

		return true;
	}

	//////////////////////////////////////////////////// TestPoint

	public function getArticles()
	{
		return $this->hasMany(Article::className(), ['user_id' => 'id']);
	}

    //////////////////////////////////////////////////// TestPoint

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /////////////////////////////////////////////////////

    public static function findByUsername($username)
    {
        return static::findOne(['name' => $username]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
