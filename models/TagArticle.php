<?php
namespace app\models;

use yii\db\ActiveRecord;

class TagArticle extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%tag_article}}';
    }

    public function rules()
    {
        return [
            [['tag_id', 'article_id'], 'required'],
            [['tag_id', 'article_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'tag_id' => 'Тег',
            'article_id' => 'Список',
        ];
    }
	public function beforeSave($insert)
	{
		$model = $this::find()->where(['tag_id' => $this->tag_id, 'article_id' => $this->article_id])->one();
		if(isset($model))
		{
			return false;
		}

		return true;
	}


    //////////////////////////////////////////////////////////////

    // relations

    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

	public function getTag()
	{
		return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
	}





}










