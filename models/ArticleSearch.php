<?php
	namespace app\models;

	use yii\base\Model;
    use yii\data\ActiveDataProvider;

	class ArticleSearch extends Article
	{
        public function rules()
        {
            return [
                [['name'], 'string'],
//                [['user_id'], 'integer'],
	            [['user_id'], 'safe']
            ];
        }

		public function search($params = null)
		{


			$query = Article::find();
//				->join('LEFT JOIN', '{{%user}}', '{{%user}}.id = {{%article}}.user_id')
//				->join('LEFT JOIN', '{{%tag}}', '{{%tag}}.id = {{%article}}.tag_id');

			$query->joinWith('user');

			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

			$this->load($params);

			if($this->validate())
			{
				$query->andFilterWhere(['like', 'name', $this->name])
				      ->andFilterWhere(['like', 'user.name', $this->user_id]);
			}

			return $dataProvider;
		}







	}
