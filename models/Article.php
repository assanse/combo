<?php
namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%article}}';
    }

    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['name'], 'string'],
            [['text'], 'safe'],
            [['user_id'], 'number', 'min' => 1],
            [['user_id'], 'integer'],
            [['tag_article_id'], 'number', 'min' => 1],
	        [['tag_article_id'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'name' => 'Заголовок',
            'text' => 'Текст',
            'user_id' => 'Пользователь',
            'tag_article_id' => 'Теги',
        ];
    }

    //////////////////////////////////////////////////////////////

    // relations
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	public function getTags()
	{
		return $this->hasMany(TagArticle::className(), ['article_id' => 'id']);
	}

	public function getTagArticle()
	{
		return $this->hasMany(TagArticle::className(), ['article_id' => 'id']);
	}

	///////////////////////////////////////////////////// TestPoint

	public function getTagList()
	{
		$tagList = [];

		foreach($this->tags as $tagArticle)
		{
			$tagList[] = $tagArticle->tag->id;
		}

		return $tagList;
	}




}










