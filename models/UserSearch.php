<?php
	namespace app\models;

    use app\models\User;
    use yii\data\ActiveDataProvider;

	class UserSearch extends Article
	{
        public function rules()
        {
            return [
                [['name'], 'string'],
            ];
        }

		public function search($params = null)
		{
			$query = User::find();

			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

			$this->load($params);

			if($this->validate())
			{
				$query->andFilterWhere(['like', 'name', $this->name]);
			}

			return $dataProvider;
		}








	}
