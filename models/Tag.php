<?php
namespace app\models;

use yii\db\ActiveRecord;

class Tag extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%tag}}';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'name' => 'Название',
        ];
    }

    //////////////////////////////////////////////////////////////

    // relations
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['tag_id' => 'id']);
    }





}










