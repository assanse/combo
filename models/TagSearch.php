<?php
	namespace app\models;

    use yii\data\ActiveDataProvider;

	class TagSearch extends Tag
	{
        public function rules()
        {
            return [
                [['name'], 'string'],
            ];
        }

		public function search($params = null)
		{
			$query = Tag::find();

			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

			$this->load($params);

			if($this->validate())
			{
				$query->andFilterWhere(['like', 'name', $this->name]);
			}

			return $dataProvider;
		}

	}
