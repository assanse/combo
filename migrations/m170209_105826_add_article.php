<?php

use yii\db\Migration;

class m170209_105826_add_article extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'text' => $this->text(),
            'tag_article_id' => $this->string(255),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('index_user_id', '{{%article}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%article}}');
    }

}
