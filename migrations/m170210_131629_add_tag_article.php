<?php

use yii\db\Migration;

class m170210_131629_add_tag_article extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tag_article}}', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer(),
            'article_id' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('tag_id', '{{%tag_article}}', 'tag_id');
        $this->createIndex('article_id', '{{%tag_article}}', 'article_id');
    }

    public function down()
    {
        $this->dropTable('{{%tag_article}}');
    }
}
