<?php
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
	use \dosamigos\ckeditor\CKEditor;
	use kartik\select2\Select2;

?>
<?php $form = ActiveForm::begin([
	'id' => 'login-form',
	'options' => ['class' => 'form-horizontal'],
	'fieldConfig' => [
		'template' => '<div class="col-lg-12">{label}</div>
                       <div class="col-lg-3">{input}</div>
                       <div class="col-lg-9">{error}</div>',
		'labelOptions' => ['class' => 'control-label'],
	],
]); ?>

	<?=Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

	<?=$form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id]); ?>
	<?=$form->field($model, 'name')->label(); ?>

	<?=Select2::widget([
		'name' => 'tag_id',
		'data' => $tagList,
		'value' => !$model->isNewRecord ? $selectList : null,
		'options' => [
			'placeholder' => 'Выбирите теги',
			'multiple' => true
		],
	]);?>

	<?=$form->field($model, 'text')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'advance'
	]) ?>

	<?=Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end() ?>
