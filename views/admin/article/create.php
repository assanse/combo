<?php
	$this->title = 'Создание';
	$this->params['breadcrumbs'] = [
        [
	        'label' => 'Статьи',
	        'url' => ['index']
        ],
		[
			'label' => $this->title,
		]
	];
?>

    <h1><?=$this->title; ?></h1>

    <?=$this->render('form', [
        'model' => $model,
        'tagList' => $tagList,
    ]); ?>
