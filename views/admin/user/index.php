<?php
    use \yii\grid\GridView;
    use \yii\helpers\Html;

	$this->title = 'Пользователи';
	$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?=$this->title; ?></h1>

<?=Html::a('Создать', ['create'], ['class' =>'btn btn-success']); ?>

<br><br>

<?=GridView::widget([
	'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,

	'emptyText' => $this->title . '&nbsp;не добавлены!',

//	'filter' => true,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		'name',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Операции',
            'template' => '{update} {delete}',
        ]
    ],

]); ?>


