<?php
	$this->title = 'Редактирование';
	$this->params['breadcrumbs'] = [
		[
			'label' => 'Теги',
			'url' => ['index']
		],
		[
			'label' => $this->title,
		]
	];
?>

    <h1><?=$this->title; ?></h1>

    <?=$this->render('form', [
        'model' => $model,

    ]); ?>
