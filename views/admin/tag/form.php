<?php
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin([
	'id' => 'login-form',
	'options' => ['class' => 'form-horizontal'],
	'fieldConfig' => [
		'template' => '<div class="col-lg-12">{label}</div>
                           <div class="col-lg-3">{input}</div>
                           <div class="col-lg-9">{error}</div>',
		'labelOptions' => ['class' => 'control-label'],
	],

]); ?>

	<?=Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

	<?=$form->field($model, 'name')->label(); ?>

	<?=Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end() ?>
