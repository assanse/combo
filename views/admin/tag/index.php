<?php
    use \yii\grid\GridView;
    use \yii\helpers\Html;
    use \yii\widgets\Pjax;
    use \app\models\User;
    use \app\models\UserSearch;

	$this->title = 'Теги';
    $this->params['breadcrumbs'][] = $this->title;
?>

<h1><?=$this->title; ?></h1>

<?=Html::a('Создать', ['create'], ['class' =>'btn btn-success']); ?>

<br><br>

<?php Pjax::begin(); ?>
<?=GridView::widget([
	'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	'emptyText' => $this->title . '&nbsp;не добавлены!',
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		'name',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Операции',
            'template' => '{update} {delete}',
        ]
    ],
]); ?>
<?php Pjax::end(); ?>


