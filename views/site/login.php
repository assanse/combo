<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $this->title = 'Вход на сайт';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="content login">

    <div class="container content-form">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
            'template' => '<div class="col-lg-12">{label}</div>
                           <div class="col-lg-3">{input}</div>
                           <div class="col-lg-9">{error}</div>',
            'labelOptions' => ['class' => 'control-label'],
            ],
        ]); ?>

            <?=$form->field($model, 'name')->textInput(['autofocus' => true]); ?>
            <?=$form->field($model, 'password')->passwordInput(); ?>
            <?=$form->field($model, 'rememberMe')->checkbox([
                'template' => '<div class="col-lg-3">{input} {label}</div><div class="col-lg-8">{error}</div>',
            ]) ?>
            <?=Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

        <?php ActiveForm::end(); ?>

    </div><!--.content-form-->

</div><!--.content.login-->
