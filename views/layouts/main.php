<?php

    /* @var $this \yii\web\View */
    /* @var $content string */

    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse'/*'navbar-inverse navbar-fixed-top'*/,
                    ],
                ]);

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        ['label' => 'Главная', 'url' => ['/site/index']],
                        ['label' => 'Теги', 'url' => ['/admin/tag/index'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Статьи', 'url' => ['/admin/article/index'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Пользователи', 'url' => ['/admin/user/index'], 'visible' => !Yii::$app->user->isGuest],
//                        ['label' => 'About', 'url' => ['/site/about']],
//                        ['label' => 'Contact', 'url' => ['/site/contact']],

                        Yii::$app->user->isGuest
                        ? ['label' => 'Логин', 'url' => ['/site/login']]
                        : ['label' => 'Выход(' . Yii::$app->user->identity->name . ')', 'url' => ['/site/logout']],
                    ]
                ]);
                NavBar::end();
            ?>

            <div class="container">
                <?=Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]); ?>
                <?= $content ?>
            </div><!--.container-->

        </div><!--.wrap-->

        <footer class="footer">

            <div class="container">
                <p class="pull-left">&copy; <?=Yii::$app->name; ?> <?=date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div><!--.container-->

        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
