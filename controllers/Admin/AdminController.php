<?php

namespace app\controllers\admin;

use app\models\TagArticle;
use Yii;
use \yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class AdminController extends Controller
{
    public $modelName = '';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	/**
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
    public function loadModel()
    {
        $id = Yii::$app->request->get('id');

        if(!is_numeric($id) && $id < 0)
        {
            throw new NotFoundHttpException('Ошибка запроса', '400');
        }
        $modelName = 'app\models\\' . $this->modelName;

        $model =  $modelName::find()->where(['id' => $id])->one();

        if(is_null($model))
        {
            throw new NotFoundHttpException('Запись не найдена', '404');
        }

        return $model;
    }



    public function actionDelete()
    {
        $this->loadModel()->delete();
        $this->redirect(['index']);
    }


	/**
	 * @param $model
	 */
	public function saveModel($model)
	{
		$post = Yii::$app->request->post($this->modelName);

		if(!empty($post))
		{
			$model->attributes = $post;

			if($model->save())
			{
				$this->redirect(['index']);
			}
		}
	}


}
