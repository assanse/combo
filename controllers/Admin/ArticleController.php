<?php

	namespace app\controllers\admin;

	use app\models\Article;
	use app\models\ArticleSearch;
	use app\models\Tag;
	use app\models\TagArticle;
	use Yii;
	use yii\helpers\ArrayHelper;
	use yii\web\NotFoundHttpException;

	class ArticleController extends AdminController
	{
	    public $modelName = 'Article';

		public function loadModel()
		{
			$id = Yii::$app->request->get('id');

			if(!is_numeric($id) && $id < 0)
			{
				throw new NotFoundHttpException('Ошибка запроса', '400');
			}
			$modelName = 'app\models\\' . $this->modelName;

			$model =  $modelName::find()->joinWith(['tags', 'tags.tag'])->where(['article.id' => $id])->one();

			if(is_null($model))
			{
				throw new NotFoundHttpException('Запись не найдена', '404');
			}

			return $model;
		}

        /**
         * @return string
         */
		public function actionIndex()
		{
            $searchModel = new ArticleSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
		}

        /**
         * @return string
         */
		public function actionCreate()
		{
			$model = new Article();

			$this->saveModel($model);

			$tagList = ArrayHelper::map(Tag::find()->all(), 'id', 'name');

			return $this->render('create', [
				'model' => $model,
				'tagList' => $tagList,
			]);
		}

        /**
         * @return string
         */
		public function actionUpdate()
		{
			$model = $this->loadModel();

			if($model->user_id != Yii::$app->user->id)
			{
				throw new NotFoundHttpException('Ошибка доступа', '403');
			}

			$tagList = ArrayHelper::map(Tag::find()->all(), 'id', 'name');
			$this->saveModel($model);
			$selectList = $model->getTagList();

			return $this->render('update', [
				'model' => $model,
				'tagList' => $tagList,
				'selectList' => $selectList,
			]);
		}


        public function actionDelete()
        {
            $model = $this->loadModel();

            if($model->user_id == Yii::$app->user->id)
            {
                $model->delete();
	            $this->redirect(['index']);
            }
            else
            {
	            throw new NotFoundHttpException('Ошибка доступа', '403');
            }
        }

		/**
		 * @param $model
		 */
		public function saveModel($model)
		{
			$post = Yii::$app->request->post($this->modelName);
			$postTags = Yii::$app->request->post('tag_id');

			if(!empty($post))
			{
				$model->attributes = $post;

				if($model->save())
				{
					foreach($postTags as $key => $tag)
					{
						$tagArticle = new TagArticle();
						$tagArticle->article_id = $model->id;
						$tagArticle->tag_id = $tag;
						$tagArticle->save();
					}

					$this->redirect(['index']);
				}
			}
		}






	}
