<?php

	namespace app\controllers\admin;

	use app\models\User;
	use app\models\UserSearch;
    use Yii;

	class UserController extends AdminController
	{
		public $modelName = 'User';

        /**
         * @return string
         */
		public function actionIndex()
		{
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
		}

        /**
         * @return string
         */
		public function actionCreate()
		{
			$model = new User();

			$this->saveModel($model);

			return $this->render('create', [
				'model' => $model
			]);
		}

		public function actionUpdate()
		{
			$model = $this->loadModel();

			return $this->render('update', [
				'model' => $model
			]);
		}

		public function actionDelete()
		{
			$this->loadModel()->delete();
            $this->redirect(['index']);
		}

		public function saveModel($model)
		{
			$post = Yii::$app->request->post($this->modelName);

			if(!empty($post))
			{
				$model->attributes = $post;
//				$model->password = Yii::$app->security->generatePasswordHash($model->password);
				//$model->rePassword = Yii::$app->security->generatePasswordHash($model->rePassword);

				if($model->save())
				{
					$this->redirect(['index']);
				}
			}
		}







	}
