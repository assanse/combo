<?php

	namespace app\controllers\admin;

	use app\models\Tag;
	use app\models\TagSearch;
	use Yii;

	class TagController extends AdminController
	{
	    public $modelName = 'Tag';

        /**
         * @return string
         */
		public function actionIndex()
		{
            $searchModel = new TagSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
		}

        /**
         * @return string
         */
		public function actionCreate()
		{
			$model = new Tag();
			$this->saveModel($model);

			return $this->render('create', [
				'model' => $model,
			]);
		}

        /**
         * @return string
         */
		public function actionUpdate()
		{
			$model = $this->loadModel();
			$this->saveModel($model);

			return $this->render('update', [
				'model' => $model
			]);
		}







	}
